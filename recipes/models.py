from django.db import models

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    image_description = models.CharField(max_length=200, null=True)
    rating = models.CharField(max_length=200, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
